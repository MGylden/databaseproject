# DatabaseProject

DatabaseProject is a program that enables the user to pull data (specifically CRUD)
from a Postgres local server. 
Furthermore it has the capabilities of searching for advanced specifics in the given database.

# Methods

All callable methods can be seen in switch case in DatabaseProjectApplication.

## Usage

CRUD related queries. Examples could be:

#Returns a customer from the database by inputting the Primary Key --
Operations.getById(INSERT INT ID HERE);

#Returns all customers in the database --
Operations.getCustomer();

#Returns a specific customer by searching for the first name -- 
Operations.getByName("INSERT NAME HERE");

#Creates a customer by inputting the specific fields --
Operations.createCustomer(ID, "firstname", "lastname", "country", "postalcode", "phone number", "email");

#Updates a customer by inputting the values you want to update - Input an id int to target the specific customer you want to update --
Operations.updateCustomer("firstname", "lastname", "country", "postalcode", "phone", "email", ID INT you want to update)

#Allows the user to limit and offset the search by inputting the specific number -- 
Operations.limitCustomerSearch(LIMIT BY INT, OFFSET BY INT) 

#Returns the country with the most customers -- 
Operations.countryWithMostCustomers();

```

Written in Collaboration with VRosendue -- Vincent Rosenørn-Due
